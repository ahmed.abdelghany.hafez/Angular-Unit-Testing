import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core'; ;

@Injectable()
export class TodoService {
  constructor(private http: HttpClient) {
  }

  add(todo) {
    return this.http.post<string>('...', todo);
  }

  getTodos() {
    return this.http.get<string[]>('...');
  }

  getTodosPromise() {
    return this.http.get('...').toPromise();
  }

  delete(id) {
    return this.http.delete('...');
  }
}
