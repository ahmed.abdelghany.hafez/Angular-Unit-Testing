import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { VoterComponent } from './voter.component';

describe('VoterComponent', () => {
  let component: VoterComponent;
  let fixture: ComponentFixture<VoterComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [VoterComponent],
    });

    fixture = TestBed.createComponent(VoterComponent);
    component = fixture.componentInstance;
  });

  it('should render the total votes', () => {
    component.othersVote = 25;
    component.myVote = 1;

    fixture.detectChanges();

    const de = fixture.debugElement.query(By.css('.vote-count'));
    const ne: HTMLElement = de.nativeElement;

    expect(ne.innerText).toContain('26');
  });

  it('should highlight the upVote button if I have upvoted', () => {
    component.myVote = 1;

    fixture.detectChanges();

    const de = fixture.debugElement.query(By.css('.glyphicon-menu-up'));

    expect(de.classes['highlighted']).toBeTruthy();
  });

  it('should increase the total votes when the user click the upvote button', () => {
    const btn = fixture.debugElement.query(By.css('.glyphicon-menu-up'));

    btn.triggerEventHandler('click', null);

    expect(component.totalVotes).toBe(1);
  });
});
