/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { UserDetailsComponent } from './user-details.component';
import { BehaviorSubject, Observable, of, Subject } from 'rxjs';
import { ActivatedRoute, Params, Router } from '@angular/router';

class RouterStub {
  navigate(params: string[]) {}
}

class ActivatedRouteStub {
  private subject? = new BehaviorSubject(null);

  push?(value) {
    this.subject.next(value);
  }

  params = this.subject.asObservable();
}

describe('UserDetailsComponent', () => {
  let component: UserDetailsComponent;
  let fixture: ComponentFixture<UserDetailsComponent>;
  let router: RouterStub;
  let activeRoute: ActivatedRouteStub;

  beforeEach(async () => {
    TestBed.configureTestingModule({
      declarations: [UserDetailsComponent],
      providers: [
        { provide: Router, useClass: RouterStub },
        { provide: ActivatedRoute, useClass: ActivatedRouteStub },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(UserDetailsComponent);
    component = fixture.componentInstance;
    router = TestBed.inject(Router);
    activeRoute = TestBed.inject(ActivatedRoute);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should navigate the user to the users component after saving', () => {
    let spy = spyOn(router, 'navigate');

    component.save();

    expect(spy).toHaveBeenCalledWith(['users']);
  });

  it('should navigate the user to the not found component', () => {
    let spy = spyOn(router, 'navigate');

    activeRoute.push({ id: 0 });

    expect(spy).toHaveBeenCalledWith(['not-found']);
  });
});
