/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HighlightDirective } from './highlight.directive';
import { By } from '@angular/platform-browser';
import { Component } from '@angular/core';

@Component({
  template: `
    <p highlight="cyan">First</p>
    <p highlight>Second</p>
  `,
})
class DirectiveHostComponent {}

describe('HighlightDirective', () => {
  let fixture: ComponentFixture<DirectiveHostComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DirectiveHostComponent, HighlightDirective],
    });
    fixture = TestBed.createComponent(DirectiveHostComponent);
    fixture.detectChanges();
  });

  it('should highlight the text with cyan color', () => {
    let nativeElement: HTMLElement = fixture.debugElement.queryAll(
      By.css('p')
    )[0].nativeElement;

    expect(nativeElement.style.backgroundColor).toBe('cyan');
  });

  it('should highlight the text with cyan color', () => {
    let directive = fixture.debugElement.queryAll(By.css('p'))[1].injector.get(HighlightDirective);
    let nativeElement: HTMLElement = fixture.debugElement.queryAll(
      By.css('p')
    )[1].nativeElement;

    expect(nativeElement.style.backgroundColor).toBe(directive.defaultColor);
  });
});
